<?php

use \WebGuy;

class HomePageCest
{
    /**
     * Checks that home page has the logo, sign up, sign in buttons and
     * does not have the main menu
     *
     * @param WebGuy $I
     */
    public function checkContent(WebGuy $I)
    {
        $I->wantTo('check that the logo, sign up and sign in are present');
        $I->amOnPage('/');
        $I->seeLink('Sign up');
        $I->seeLink('Sign in');
    }

    /**
     * Checks for all metas on the home page
     *
     * @param WebGuy $I
     */
    public function checkMetas(WebGuy $I)
    {
        $I->wantTo('check that all the meta tags are present');
        $I->amOnPage('/');
        $I->seeElementInDOM('meta[name="title"]');
        $I->seeElementInDOM('meta[name="description"]');
        $I->seeElementInDOM('meta[name="keywords"]');
        $I->seeElementInDOM('meta[property="og:title"]');
        $I->seeElementInDOM('meta[property="og:description"]');
        $I->seeElementInDOM('meta[property="og:image"]');
    }

    /**
     * Checks that the main menu element is not in the DOM
     *
     * @param WebGuy $I
     */
    public function checkMainMenuIsNotInDOM(WebGuy $I)
    {
        $I->wantTo('ensure menu is not present in DOM');
        $I->amOnPage('/');
        $I->dontSeeElementInDOM('#main-menu');
    }

    /**
     * Check for Google Analytics on page
     *
     * @param WebGuy $I
     */
    public function checkForGoogleAnalyticsInDOM(WebGuy $I)
    {
        $I->wantTo('check that Google analytics is present on page');
        $I->amOnPage('/');
        $I->seeInPageSource('UA-52164821-1');
    }

}