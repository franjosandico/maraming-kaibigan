@extends('layouts._wrapper')

{{-- Keep in single line please --}}
@section('bodyClass') public @stop

@section('layout')
    <?php //Main menu, should only show on home page, handled by javascript --> ?>
    @if (!isset($hideMenu))
        @include('partials.main-menu')
    @endif

    <div class="content-wrapper">
        <!-- Include page title if the variable is set -->
        @if (isset($pageTitle))
            @include('partials.page-title')
        @endif

        <!-- Content goes here -->
        @yield('content')
    </div>

    <!-- Footer -->
    @include('partials.footer')

    <!-- Public facing modals -->
    @include('modals.sign-in')
@stop

