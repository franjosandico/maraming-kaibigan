@extends('layouts.public')

@section('content')


<div class="container">

@if (isset($status))
	<div class="success">Contact Form Passed!</div>
@endif
@if (count($errors) > 0)
	@foreach ($errors as $error)
		<div class="error">{{$error}}</div>
	@endforeach
@endif

{{Form::open(array('method' => 'post'))}}

{{Form::label('first_name', 'First Name')}}
{{Form::text('first_name', Input::old('first_name'), array('required'))}}

{{Form::label('surname', 'Surname')}}
{{Form::text('surname', Input::old('surname'), array('required'))}}

{{Form::label('email', 'E-mail')}}
{{Form::text('email', Input::old('email'), array('required'))}}

{{Form::label('daytime_contact_number', 'Daytime Contact Number')}}
{{Form::text('daytime_contact_number', Input::old('daytime_contact_number'), array('required'))}}

{{Form::label('address', 'Address')}}
{{Form::text('address', Input::old('address'), array('required'))}}

{{Form::label('suburb', 'Suburb')}}
{{Form::text('suburb', Input::old('suburb'), array('required'))}}

{{Form::label('state', 'State')}}
{{Form::select('state', array('ACT' => 'Australian Capital Territory', 'NSW' => 'New South Wales', 'NT' => 'Northern Territory', 'QLD' => 'Queensland', 'SA' => 'South Australia', 'TAS' => 'Tasmania', 'VIC' => 'Victoria', 'WA' => 'Western Australia'))}}

{{Form::label('postcode', 'Postcode')}}
{{Form::text('postcode', Input::old('postcode'), array('required'))}}

{{Form::label('enquiry_type', 'Enquiry Type')}}
{{Form::select('enquiry_type', array('General enquiry' => 'General enquiry', 'Product feedback or enquiry' => 'Product feedback or enquiry', 'Product complaint' => 'Product complaint'))}}

{{Form::label('product_name', 'Product Name')}}
{{Form::text('product_name')}}

{{Form::label('product_size', 'Product Size')}}
{{Form::text('product_size')}}

{{Form::label('use_by_date', 'Use-by Date')}}
{{Form::text('use_by_date')}}

{{Form::label('batch_code', 'Batch Code')}}
{{Form::text('batch_code')}}

{{Form::label('enquiry', 'Enquiry')}}
{{Form::textarea('enquiry')}}

<div class='form-action'>
{{Form::submit('Submit &gt;')}}
</div>

{{Form::close()}}
</div>


<style type="text/css">
div.container {
	width: 60%;
	margin: 0 auto;
	padding: 2em;
}

div.success {
	background-color: lightgreen;
	padding: 1em;
}

div.error {
	background-color: red;
	padding: .5em;
	color: white;
}

label {
	width: 50% !important;
}
input, select, textarea{
	border-radius:.5em;
}
</style>

@stop